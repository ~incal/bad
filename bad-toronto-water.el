;;; -*- lexical-binding: t -*-

(require 'cl-lib)
(require 'eieio)
(require 'subr-x)

(require 'bad-cyclic)

(cl-defmethod bad-water ((c cyclic))
  (with-slots (src) c
   (concat " "
    (seq--into-string
     (cl-substitute ?~ ?1
      (cl-substitute ?- ?0 (pop src)))))))

(cl-defmethod bad-lake ((c cyclic))
  (with-slots (w h data len) c
    (let* ((chars (* w h))
           (bytes (1+ (/ chars 8))))
      (setf data
            (string-to-list
             (string-join
              (cl-loop
               repeat bytes
               collect (bad-water c)))))
      (setf len (length data)))))

(provide 'bad-toronto-water)
