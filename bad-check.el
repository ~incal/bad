;;; -*- lexical-binding: t -*-

(defun bad-check-version ()
  (when (version< emacs-version "29.0.0")
    (user-error "You need to have Emacs 29 or later for this.")))

(defun bad-check-res ()
  (delete-other-windows)
  (pcase-let ((`(,_ ,_ ,cols ,lines) (window-edges))
              (cols-needed  78)
              (lines-needed 28))
    (when (or (< cols cols-needed)
              (< lines lines-needed))
      (user-error "You need to have %d columns and %d lines (you have %dx%d)"
                  cols-needed lines-needed
                  cols lines))))

(defun bad-check ()
  (bad-check-version)
  (bad-check-res))

(provide 'bad-check)
