;;; -*- lexical-binding: t -*-

(require 'eieio)

(require 'bad-ascii)
(require 'bad-borderless)
(require 'bad-box)
(require 'bad-box-draw)
(require 'bad-caption)
(require 'bad-demo)
(require 'bad-elem)

;;  78 x 28 works with Agave:pixelsize=16
;;  91 x 30 works with Agave:pixelsize=14
;;  93 x 38 ???        Agave:pixelsize=12
;; 127 x 42 works with Agave:pixelsize=10

(defclass game (elem)
  ((name :initform "bad/draw game")
   (w    :initform #1=78)
   (h    :initform #2=28)
   (buf    :initarg :buf    :type buffer
           :initform (get-buffer-create "*bad-buffer*"))
   (screen :initarg :screen :type box
           :initform (box :name "screen"
                          :w  #1#
                          :h  #2#
                          :c1 #1=?\s :s1 #2='(#1#) :c2 #1#
                          :s4 #2#                  :s2 #2#
                          :c4 #1#    :s3      #2#  :c3 #1#))
   (bg    :initarg :bg    :type (or null ascii)      :intiform nil)
   (field :initarg :field :type (or null borderless) :initform nil)
   (help  :initarg :help  :type (or null caption)    :initform nil)
   (tmr   :initarg :tmr   :type (or null vector)     :initform nil)))

(cl-defmethod bad-game-set-field ((g game) &optional w h)
  (or w (setq w (/ (* (frame-char-width)  (frame-width))  (frame-char-width))))
  (or h (setq h (/ (* (frame-char-height) (frame-height)) (frame-char-height))))
  (with-slots ((wg w) (hg h)) g
    (setf wg w)
    (setf hg h))
  (message "Field size: %d x %d" w h))

(cl-defmethod bad-help-toggle ((g game))
  (with-slots (help) g
    (when help
      (bad-switch help)
      (bad-update g))))

(cl-defmethod bad-setup ((g game))
  (with-slots (buf) g
    (with-current-buffer buf
      (bad-demo-mode)
      (bad-demo-prepare))
    (pop-to-buffer buf)
    (delete-other-windows)))

(cl-defmethod bad-quit ((g game))
  (with-slots (tmr) g
    (when (timerp tmr)
      (cancel-timer tmr))
    (kill-emacs)))

(provide 'bad-game)
