;;; -*- lexical-binding: t -*-

(require 'eieio)

(require 'bad-box)
(require 'bad-elem)
(require 'bad-size)
(require 'bad-solid)
(require 'bad-write)

(defclass triangle (solid)
  ((name :initform (format "triangle-%d" (random 100)))))

(cl-defmethod bad-update ((tri triangle) &optional init)
  (when init
    (with-slots (w h data len spc) tri
      (setf data
        (cl-loop
         for l from 0 to (1- h)
         for c from w downto 1
         for p from 0 to (1- h)
         collect (concat (make-list c (car spc)) (make-list p bad-nonsolid)) into res
         finally return (string-to-list (string-join res))))
      (setf len (length data)))))

(cl-defmethod bad-size :after ((tri triangle) &rest _)
  (with-slots (w h) tri
    (setf h w))
  (bad-update tri t))

(cl-defmethod bad-add ((_ triangle) (_ elem)) (ignore))

(cl-defmethod bad-add ((b box) (tri triangle))
  (with-slots (w h sub) b
    (with-slots ((iw w) (ih h) w-max h-max x y min-x min-y) tri
      (setf iw #1=3)
      (setf ih #1#)
      (setf w-max (- w 2))
      (setf h-max (- h 2))
      (setf x #3=1)
      (setf y #4=1)
      (setf min-x #3#)
      (setf min-y #4#))
    (bad-update tri t)
    (push tri sub)
    tri))

(provide 'bad-triangle)
