;;; -*- lexical-binding: t -*-

(require 'bad-elem)
(require 'bad-move)
(require 'bad-pos)

(cl-defmethod bad-align-c-sub ((e elem) (dx integer) (dy integer))
  (with-slots (w h sub) e
    (dolist (s sub)
      (with-slots (x y align-c) s
        (when align-c
          (let* ((do-x (zerop (mod w 2)))
                 (do-y (zerop (mod h 2)))
                 (hdx  (+ (/ dx 2) (or (and (if (< 0 dx) do-x (not do-x)) (% dx 2)) 0)))
                 (hdy  (+ (/ dy 2) (or (and (if (< 0 dy) (not do-y) do-y) (% dy 2)) 0))))
            (bad-mov s hdx hdy)))))))

(cl-defmethod bad-align-rel-xy-sub ((e elem))
  (with-slots (w h sub) e
    (dolist (s sub)
      (with-slots (x y align-rel-xy) s
        (when align-rel-xy
          (let ((px (car  align-rel-xy))
                (py (cadr align-rel-xy)))
            (bad-pos s (or (and px (+ w px)) x)
                       (or (and py (+ h py)) y))))))))

(cl-defmethod bad-align-sub ((e elem) (dx integer) (dy integer))
  (bad-align-c-sub e dx dy)
  (bad-align-rel-xy-sub e))

(provide 'bad-align)
