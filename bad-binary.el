;;; -*- lexical-binding: t -*-

(require 'subr-x)

(defun bad--number-to-bits (num)
  (let ((s ""))
    (while (< 0 num)
      (setq s (concat (number-to-string (logand num 1)) s))
      (setq num (ash num -1)) )
    (if (string= "" s)
        "0"
      s)))

(defun bad--string-to-bytes (str &optional delim)
  (or delim (setq delim " "))
  (let ((data))
    (dolist (c (string-to-list str))
      (push (string-pad (bad--number-to-bits c) 8 ?0 t)
            data))
    (nreverse data)))

(defun bad-bytes (str)
  (let ((bytes (string-to-list (bad--string-to-bytes str))))
    (setcdr (last bytes) bytes)))

(provide 'bad-binary)
