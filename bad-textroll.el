;;; -*- lexical-binding: t -*-

(require 'eieio)

(require 'bad-caption)
(require 'bad-pos)

(defclass textroll (caption)
  ((name :initform "textroll")
   (halted :initarg :halted :custom boolean :initform nil)
   (more   :initarg :more   :type   string  :initform "")))

(cl-defmethod bad-type ((tr textroll))
  (with-slots (halted text-draw more) tr
    (unless (or halted (string= "" more))
      (let ((next (seq-take more 1)))
        (if (string= next "\n")
            (setq halted t)
          (bad-text tr (list text-draw next))))
      (setf more (seq-rest more)))))

(cl-defmethod bad-text ((tr textroll) (strs list))
  (with-slots (text-draw data len x y w) tr
    (bad-pos tr x y)
    (setf text-draw (string-join strs))
    (setf data (string-to-list text-draw))
    (setf len (length data))))

(cl-defmethod bad-set ((tr textroll) (s string))
  (with-slots (more) tr
    (setf more s)))

(provide 'bad-textroll)
