;;; -*- lexical-binding: t -*-
;;
;; this file:
;;   https://dataswamp.org/~incal/bad/bad-vertical.el

(require 'eieio)

(require 'bad-ascii)
(require 'bad-borderless)
(require 'bad-box-draw)
(require 'bad-draw)
(require 'bad-rotate)

;; o 1
;; x 2
;; o 3
;; x 4

(defclass vertical (ascii)
  ((hit :initarg :hit :type (or null integer) :initform nil)))

(cl-defmethod bad-find-vertical ((v vertical) file str)
  (bad-read-file v file "" t)
  (bad-transpose v)
  (let ((buf (get-buffer-create "*vertical*")))
    (with-current-buffer buf
      (bad-draw-to-buf v)
      (pop-to-buffer buf)
      (re-search-forward str))))

(cl-defmethod bad-run ((v vertical))
  (let ((search-str (read-string "search for: ")))
    (bad-find-vertical v "~/.emacs" search-str)))

;; (eval-buffer)
;; (bad-run (vertical))

(provide 'bad-vertical)
