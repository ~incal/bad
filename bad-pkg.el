;;; -*- lexical-binding: t -*-

(define-package "bad" "2.0.0" "ascii art game engine"
  '((emacs "29"))
  :authors
  '("Emanuel Berg" . "incal@dataswamp.org")
  :maintainer
  '("Emanuel Berg" . "incal@dataswamp.org")
  :keywords
  '("games" "terminal")
  :url
  "https://dataswamp.org/~incal/bad")

;; Local Variables:
;; no-byte-compile: t
;; End:
