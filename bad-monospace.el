;;; -*- lexical-binding: t -*-
;;
;; this file:
;;   https://dataswamp.org/~incal/monospace.el

(let ((i 0)
      (fonts (list
              "Agave:pixelsize=16"
              "Agave:pixelsize=14"
              "Agave:pixelsize=12"
              "Agave:pixelsize=10"
              "DejaVu Sans Mono:pixelsize=12"
              "DejaVu Sans Mono:bold:pixelsize=12"
              "Fira Code:pixelsize=12"
              "Fira Code:bold:pixelsize=12"
              "Go Mono:pixelsize=12"
              "Hack:pixelsize=12"
              "Hack:bold:pixelsize=12"
              "Liberation Mono:pixelsize=12"
              "PT Mono:pixelsize=12"
              )))
  (defun bad-set-monospace-font ()
    (interactive)
    (let ((font (nth i fonts)))
      (set-frame-font font)
      (setq i (mod (cl-incf i) (length fonts)))
      (message font))))

;; not monospace:
;;  "-xos4-terminus-bold-r-normal--14-160-72-72-c-80-iso10646-1"
;;  "-xos4-terminus-bold-r-normal--16-160-72-72-c-80-iso10646-1"

(declare-function bad-set-monospace-font nil)

(provide 'bad-monospace)
