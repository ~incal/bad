;;; -*- lexical-binding: t -*-

(require 'eieio)

(require 'bad-borderless)
(require 'bad-box)
(require 'bad-elem)
(require 'bad-write)

(defclass food (elem)
  ((name :initform "food")
   (c    :initarg :c :type integer :initform ?$)))

(cl-defmethod bad-init ((f food) &optional dst) ; NOTE: OK
  (when (cl-typep dst 'borderless)
    (with-slots (w h) dst
      (with-slots (min-x min-y max-x max-y) f
        (let ((brd (if (box-p dst) 1 0)))
          (setf min-x brd)
          (setf min-y brd)
          (setf max-x (- w brd 1))
          (setf max-y (- h brd 1)))))))

(cl-defmethod bad-update ((f food) &optional dst) ; NOTE: OK
  (when (cl-typep dst 'borderless)
    (with-slots (x y max-x max-y c) f
      (unless (and max-x max-y)
        (bad-init f dst))
      (with-slots (w) dst
        (bad-write-index dst (+ x (* w y)) c)))))

(provide 'bad-snake-food)
