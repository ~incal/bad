;;; -*- lexical-binding: t -*-

(require 'eieio)
(require 'bad-elem)

(cl-defmethod bad-pos ((e elem) (new-x integer) &optional new-y)
  (or new-y (setq new-y (oref e y)))
  (with-slots (movable min-x min-y x y max-x max-y) e
    (when (and movable
               (<= 0 min-x new-x (or max-x new-x))
               (<= 0 min-y new-y (or max-y new-y)))
      (setf x new-x)
      (setf y new-y))))

(cl-defmethod bad-pos-x ((e elem) (new-x integer))
  (bad-pos e new-x))

(cl-defmethod bad-pos-y ((e elem) (new-y integer))
  (with-slots (x) e
    (bad-pos e x new-y)))

;; -----------------------------------------------------------------------

(cl-defmethod bad-origo ((e elem))
  (with-slots (min-x min-y) e
    (bad-pos e min-x min-y)))

(cl-defmethod bad-center ((e elem))
  (with-slots (w-max w min-y) e
    (let ((wm (or w-max 78)))
      (bad-pos e (- (/ wm 2) (/ w 2)) min-y))))

;; -----------------------------------------------------------------------

(cl-defmethod bad-pos-inside ((inner elem) (outer elem))
  (with-slots ((ow w) (oh h)) outer
    (with-slots (ix iy min-x min-y (bx x) (by y) (bw w) (bh h)) inner
      (let* ((max-scale 2)
             (x-mas (- ow bw))
             (y-mas (- oh bh)))
        (setf bx (+ (* ix (/ x-mas max-scale))
                    (if (zerop ix)
                        min-x
                      (if (= ix max-scale)
                          (- (% bw max-scale) 1)
                       0))))
        (setf by (+ (* iy (/ y-mas max-scale))
                    (if (zerop iy)
                        min-y
                      (if (= iy max-scale)
                          (- (% bh max-scale) 1)
                       0))))
        (setf ix (mod (cl-incf ix) #1=(1+ max-scale)))
        (when (zerop ix)
          (setf iy (mod (cl-incf iy) #1#)))))))

(provide 'bad-pos)
