;;; -*- lexical-binding: t -*-

(require 'eieio)
(require 'bad-borderless)

(defclass solid (borderless)
  ((name  :initform (format "solid-%d" (random 100)))
   (fg    :initform (seq-random-elt (defined-colors)))
   (spc   :initform '(?#))
   (w     :initform #1=3)
   (h     :initform #1#)
   (w-min :initform #1#)
   (h-min :initform #1#)))

(provide 'bad-solid)
