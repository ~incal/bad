;;; -*- lexical-binding: t -*-

(require 'eieio)

(defclass elem ()
  ((name         :initarg :name         :type   string             :initform (format "element-%d" (random 100)))
   (deletable    :initarg :deletable    :custom boolean            :initform t)
   (movable      :initarg :movable      :custom boolean            :initform t)
   (transposable :initarg :transposable :custom boolean            :initform t)
   (deleted      :initarg :deleted      :custom boolean            :initform nil)
   (visible      :initarg :visible      :custom boolean            :initform t)
   (fg           :initarg :fg           :type   (or null string)   :initform nil)
   (color        :initarg :color        :type   (or null string)   :initform nil) ; TODO: color -> bg
   (data-str     :initarg :list         :type   list               :initform nil)
   (data         :initarg :data         :type   list               :initform nil)
   (len          :initarg :len          :type   integer            :initform 0)
   (ix           :initarg :ix           :type   integer            :initform 0)
   (iy           :initarg :iy           :type   integer            :initform 0)
   (min-x        :initarg :min-x        :type   integer            :initform 0)
   (min-y        :initarg :min-y        :type   integer            :initform 0)
   (x            :initarg :x            :type   integer            :initform 0)
   (y            :initarg :y            :type   integer            :initform 0)
   (max-x        :initarg :max-x        :type   (or null integer)  :initform nil)
   (max-y        :initarg :max-y        :type   (or null integer)  :initform nil)
   (align-c      :initarg :align-c      :custom boolean            :initform nil)
   (align-rel-xy :initarg :align-rel-xy :type   list               :initform nil)
   (w-min        :initarg :w-min        :type   integer            :initform 1)
   (h-min        :initarg :h-min        :type   integer            :initform 1)
   (w            :initarg :w            :type   integer            :initform 0)
   (h            :initarg :h            :type   integer            :initform 0)
   (w-max        :initarg :w-max        :type   integer            :initform 78)
   (h-max        :initarg :h-max        :type   integer            :initform 28)
   (pushy        :initarg :pushy        :custom boolean            :initform nil)
   (resize-wh    :initarg :resize-wh    :type   list               :initform nil)
   (tick         :initarg :tick         :type   (or null function) :initform nil)
   (sub          :initarg :sub          :type   list               :initform nil)))

(cl-defmethod bad-info ((e elem))
  (when nil
    (with-slots (name x y w h fg color) e
      (message
       "%s (%d %d) %dx%d chars, color: [fg %s] [bg %s]"
       name
       x y
       w h
       (if fg    (propertize fg    'face (list :foreground fg))    "nil")
       (if color (propertize color 'face (list :background color)) "nil")))))

(provide 'bad-elem)
