;;; -*- lexical-binding: t -*-

(require 'eieio)
(require 'bad-ascii)

(defclass caption (ascii)
  ((name      :initform (format "caption-%d" (random 100)))
   (text-draw :initarg :text-draw :type string :initform "")
   (text      :initarg :text      :type string :initform "")
   (text-alt  :initarg :text-alt  :type string :initform "")))

(cl-defmethod bad-draw-open ((c caption))
  (with-slots (text-draw text) c
    (setf text-draw text))
  (bad-update c t))

(cl-defmethod bad-draw-fold ((c caption))
  (with-slots (text-draw text-alt) c
    (setf text-draw text-alt))
  (bad-update c t))

(cl-defmethod bad-switch ((c caption))
  (with-slots (text-draw text text-alt) c
    (if (string= text text-draw)
        (setf text-draw text-alt)
      (setf text-draw text)))
  (bad-update c t))

(cl-defmethod bad-update ((c caption) &optional force)
  (with-slots (data len text-draw w h) c
    (when (or force (not data))
      (setf data (string-to-list text-draw))
      (setf len #1=(length data)))
    (when (zerop h)
      (setf h 1))
    (when (and (= h 1) (zerop w))
      (setf w #1#))))

(provide 'bad-caption)
