;;; bad.el --- interactive graphics game engine -*- lexical-binding: t -*-
;;
;; Author: Emanuel Berg <incal@dataswamp.org>
;; Created: 2024-08-24
;; Git: git clone https://dataswamp.org/~incal/bad.git
;; Keywords: games, terminals
;; License: GPL3+
;; URL: https://dataswamp.org/~incal/bad
;; Version: 2.0.0
;;
;;; Commentary:
;;   _________________________________________
;;  |  _____     _____        _      _     _  |
;;  | |  __ \   |  __ \      / \    | | _ | | |
;;  | | |  | |  | |__) |    / _ \   | |/ \| | |
;;  | | |__| |  |  __ <    / ___ \  |   _   | |
;;  | |_____/   |_|  |_|  /_/   \_\ |__/ \__| |
;;  |                                         |
;;  | dorks run away in the face of a warrior |
;;  |_________________________________________|
;;
;;  Ascii art game drawing program and game
;;  engine for Emacs.
;;
;;  By incal <incal@dataswamp.org> 2024.
;;
;;  See the README or do M-x eval-buffer and
;;  then M-x any of the functions, below.
;;
;;; Code:

(require 'cl-lib)
(cl-pushnew "." load-path :test #'string=)
(require 'bad-paths)

;; keep

(require 'bad-demo-draw-here)
(require 'bad-demo-mini)

(require 'bad-egypt)
(require 'bad-grad)
(require 'bad-maple)
(require 'bad-snake-game)
(require 'bad-studio)
(require 'bad-toronto)

(defun bad-egypt ()
  (interactive)
  (bad-run (egypt)))

(defun bad-studio ()
  (interactive)
  (bad-run (studio)))

(defun bad-toronto ()
  (interactive)
  (bad-run (toronto)))

(defun bad-snake ()
  (interactive)
  (bad-run (snake-game)))

(defun bad-grr ()
  (interactive)
  (bad-run (grr)))

(defun bad-maple ()
  (interactive)
  (bad-run (maple)))

(provide 'bad)
;;; bad.el ends here
