;;; -*- lexical-binding: t -*-

(require 'eieio)
(require 'bad-borderless)

(defclass anim (borderless)
  ((name :initform (format "animation-%d" (random 100)))
   (frames :initarg :frames :type list    :initform nil)
   (num    :initarg :num    :type integer :initform 0)
   (f      :initarg :f      :type integer :initform 0)))

(cl-defmethod bad-add-frame ((a anim) (f borderless))
  (with-slots (frames n num) a
    (setf frames `(,@frames ,f))
    (setf num (length frames))))

(cl-defmethod bad-next-frame ((a anim))
  (with-slots (f num frames) a
    (setf f (mod (cl-incf f) num))
    (mapc (lambda (e)
            (with-slots (visible) e
              (setf visible nil)))
          frames)
    (setf (oref (nth f frames) visible) t)))

(cl-defmethod bad-update ((a anim))
  (bad-next-frame a))

(provide 'bad-animation)
