;;; -*- lexical-binding: t -*-

(require 'eieio)
(require 'bad-elem)

(cl-defmethod bad-transpose-pos ((e elem))
  (with-slots (x y) e
    (cl-rotatef x y)))

(cl-defmethod bad-flip-x-pos ((e elem))
  (with-slots (w-max w x min-x) e
    (setf x (+ (- w-max w x) min-x 1))))

(cl-defmethod bad-flip-y-pos ((e elem))
  (bad-flip-x-pos e)
  (with-slots (h-max h y min-y) e
    (setf y (+ (- h-max h y) min-y 1))))

(cl-defmethod bad-transpose ((e elem) &optional inner)
  (when (or (oref e transposable) inner)
    (with-slots (data w h sub) e
      (let ((data-lists (cl-loop
                         with chunks = (seq-split data w)
                         for c in chunks
                         collect c)))
        (setf data
              (flatten-list (cl-loop
                             for col-i below w
                             collect (cl-loop
                                      for row in data-lists
                                      collect (elt row col-i))))))
      (cl-rotatef w h)
      (when inner
        (bad-transpose-pos e))
      (mapc (lambda (s) (bad-transpose s t)) sub))))

(cl-defmethod bad-flip-x ((e elem) &optional inner)
  (with-slots (data w sub) e
    (setf data (string-to-list
                (string-join (cl-loop
                              with chunks = (seq-split data w)
                              for c in chunks
                              collect (nreverse c)))))
    (when inner
      (bad-flip-x-pos e))
    (mapc (lambda (s) (bad-flip-x s t)) sub)))

(cl-defmethod bad-flip-y ((e elem) &optional inner)
  (with-slots (data sub) e
    (setf data (nreverse data))
    (bad-flip-x e)
    (when inner
      (bad-flip-x e)
      (bad-flip-y-pos e))
    (mapc (lambda (s) (bad-flip-y s t)) sub)))

(cl-defmethod bad-rotate-back ((e elem) &optional n)
  (or n (setq n 1))
  (bad-rotate e (- n)))

(cl-defmethod bad-rotate ((e elem) &optional n inner)
  (or n (setq n 1))
  (when (< n 0)
    (setq n (* (abs n) 3)))
  (with-slots (sub movable) e
    (when movable
      (cl-loop
       repeat n do
       (unless inner
         (bad-transpose e)
         (bad-flip-x e))
       (mapc (lambda (s) (bad-rotate s 3 t)) sub)))))

(provide 'bad-rotate)
