;;; -*- lexical-binding: t -*-

(require 'eieio)
(require 'bad-ascii)

(defun bad-list-shift (l)
  (append (last l) (butlast l)))

(defclass cyclic (ascii)
  ((name    :initform "cyclic")
   (src     :initarg :src     :type   list    :initform nil)
   (enabled :initarg :enabled :custom boolean :initform t)))

(cl-defmethod bad-toggle ((c cyclic) &optional feat)
  (or feat (setq feat "feature"))
  (with-slots (enabled) c
    (setf enabled (not enabled))
    (message "%s %s%s" feat (if enabled "ena" "disa") "bled")))

(cl-defmethod bad-update ((c cyclic))
  (with-slots (enabled data) c
    (when enabled
      (setf data (bad-list-shift data)))))

(provide 'bad-cyclic)
