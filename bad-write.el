;;; -*- lexical-binding: t -*-

(require 'eieio)
(require 'bad-elem)

(defconst bad-nonsolid ?{)

(cl-defmethod bad-write-subs ((e elem))
  (dolist (s (oref e sub))
    (bad-write-replace e s)))

(cl-defmethod bad-write-index ((src elem) (i integer) (dst integer))
  (with-slots (data) src
    (setf (nth i data) dst)))

(cl-defmethod bad-write-replace ((dst elem) (src elem) &optional read)
  (with-slots (data w h) dst
    (with-slots
        ((src-data data)
         (src-len len)
         (src-w w)
         (src-h h)
         (src-x x)
         (src-y y)
         (src-visible visible))
        src
      (when (and data src-data src-visible)
        (cl-loop
         with read-data
         with loop-data = (take (* src-h src-w) src-data)
         for i from 0 to (1- src-len)
         for s in loop-data do
         (let* ((pos-x (+ src-x (mod i src-w)))
                (pos-y (* (+ src-y (/ i src-w))))
                (pos   (+ pos-x (* pos-y w))))
           (when (and (< pos-x w)
                      (< pos-y h))
             (if read
                 (when (= s bad-nonsolid)
                   (push (nth pos data) read-data))
               (when (not (= s bad-nonsolid))
                 (setf (nth pos data) s)))))
         finally return (when read (nreverse read-data)))))))

(provide 'bad-write)
