;;; -*- lexical-binding: t -*-

(require 'eieio)

(require 'bad-align)
(require 'bad-elem)
(require 'bad-move)
(require 'bad-pos)

(cl-defmethod bad-resize-sub ((e elem))
  (with-slots (sub w h) e
    (dolist (s sub)
      (with-slots (resize-wh (sub-w w) (sub-h h) w-max h-max) s
        (setf w-max w)
        (setf h-max h)
        (when resize-wh
          (let ((rx (car  resize-wh))
                (ry (cadr resize-wh)))
          (bad-size s (or (and rx (+ w rx)) sub-w)
                      (or (and ry (+ h ry)) sub-h))))))))

(cl-defmethod bad-size ((e elem) (new-w integer) &optional new-h)
  (or new-h (setq new-h new-w))
  (with-slots (movable sub
               w w-min w-max
               h h-min h-max)
      e
    (when (and movable
               (or (not w-max) (<= w-min new-w w-max))
               (or (not h-max) (<= h-min new-h h-max)))
      (let ((dx (- new-w w))
            (dy (- new-h h)))
        (setf w new-w)
        (setf h new-h)
        (bad-align-sub e dx dy)
        (bad-resize-sub e)))))

(cl-defmethod bad-size-min ((e elem))
  (with-slots (min-x min-y w-min h-min) e
    (bad-pos e min-x min-y)
    (bad-size e w-min h-min)))

(cl-defmethod bad-size-half ((e elem))
  (with-slots (min-x min-y w h w-max h-max) e
    (bad-pos e min-x min-y)
    (let ((new-w (/ (or w-max w) 2))
          (new-h (/ (or h-max h) 2)))
    (bad-size e new-w new-h))))

(cl-defmethod bad-size-square ((e elem))
  (with-slots (w h w-max h-max) e
    (let ((small (min (or w-max w) (or h-max h))))
      (bad-size e small small))))

(cl-defmethod bad-size-max ((e elem))
  (with-slots (min-x min-y w h w-max h-max) e
    (bad-pos e min-x min-y)
    (bad-size e (or w-max (* #1=2 w))
                (or h-max (* (1- #1#)  h)))))

(cl-defmethod bad-size-add ((e elem) &optional x y)
  (or x (setq x 1))
  (or y (setq y x))
  (with-slots (w h) e
    (bad-size e (+ x w) (+ y h))))

(cl-defmethod bad-add-col ((e elem) &optional n)
  (or n (setq n 1))
  (bad-size-add e n 0))

(cl-defmethod bad-add-row ((e elem) &optional n)
  (or n (setq n 1))
  (bad-size-add e 0 n))

(provide 'bad-size)
