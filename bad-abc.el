;;; -*- lexical-binding: t -*-

(require 'eieio)

(require 'bad-ascii)
(require 'bad-elem)
(require 'bad-pos)
(require 'bad-write)

(defclass abc (ascii)
  ((name :initform "abc")
   (dl :name "d" :initarg :dl :type (or null ascii) :initform nil)
   (rl :name "r" :initarg :rl :type (or null ascii) :initform nil)
   (al :name "a" :initarg :al :type (or null ascii) :initform nil)
   (wl :name "w" :initarg :wl :type (or null ascii) :initform nil)))

(cl-defmethod bad-draw-draw ((a abc) (e elem) &optional x y)
  (with-slots (dl rl al wl) a
    (when (or x y)
      (let ((dist (1+ (oref wl w))))
        (bad-pos dl    x             y)
        (bad-pos rl (+ x      dist)  y)
        (bad-pos al (+ x (* 2 dist)) y)
        (bad-pos wl (+ x (* 3 dist)) y)))
    (bad-write-replace e dl)
    (bad-write-replace e rl)
    (bad-write-replace e al)
    (bad-write-replace e wl)))

(cl-defmethod bad-init ((a abc))
  (with-slots (dl rl al wl) a
    (setf dl (ascii :name "d"))
    (setf rl (ascii :name "r"))
    (setf al (ascii :name "a"))
    (setf wl (ascii :name "w"))
    (let ((dir (file-name-concat "data" "abc")))
      (bad-read-file dl nil dir)
      (bad-read-file rl nil dir)
      (bad-read-file al nil dir)
      (bad-read-file wl nil dir))))

(provide 'bad-abc)
