;;; -*- lexical-binding: t -*-

(require 'bad-monospace)

(defvar bad-demo-mode-map (make-keymap))

(define-minor-mode bad-demo-mode "bad demo mode")

(defun bad-demo-fullscreen ()
  (setq frame-resize-pixelwise t)
  ;; maybe this line is meaningless, but if it is, might as well do it
  (set-face-attribute 'fringe nil :background #1="black" :foreground #1#)
  (fringe-mode -1)
  (set-frame-parameter nil 'fullscreen 'fullboth))

(defun bad-demo-font ()
  (font-lock-mode -1)
  (set-foreground-color "white")
  (set-background-color "gray0")
  (keymap-set bad-demo-mode-map "`" #'bad-set-monospace-font)
  (let ((font "Agave:pixelsize=16"))
    (condition-case nil
        (set-frame-font font nil t)
      (error (message "Damn! Font not found: %s" font)))
    (set-fontset-font t 'unicode (font-spec :name "freemono") nil 'append)))

(defun bad-demo-hide ()
  (setq cursor-type      nil)
  (setq mode-line-format nil)
  (blink-cursor-mode     -1)
  (menu-bar-mode         -1)
  (scroll-bar-mode       -1)
  (show-paren-mode       -1)
  (toggle-truncate-lines -1)
  (tool-bar-mode         -1)
  (visual-line-mode      -1))

(defun bad-demo-disable ()
  (buffer-disable-undo)
  (setq extended-command-suggest-shorter nil))

(defun bad-demo-prepare (&optional force)
  (let ((inhibit-message t))
    (when (or force (boundp 'bad-demo-run-from-script))
      (bad-demo-fullscreen)
      (bad-demo-font)
      (bad-demo-hide)
      (bad-demo-disable))))

(provide 'bad-demo)
