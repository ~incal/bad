;;; -*- lexical-binding: t -*-

(require 'eieio)

(require 'bad-box)
(require 'bad-box-draw)
(require 'bad-write)

;;           | 8+2=10 |__
;;           |        |
;; ?a = 97 =  01100001   3*10 = 30
;;                    |__
;;            76543210
;;
;; (3 + 1) * (10 + 1) = 4*11 = 44

(defclass magni-glass (box)
  ((name  :initform "magni-glass")
   (w-min :initform 4)
   (h-min :initform 3)
   (w     :initform 7)
   (h     :initform 3)
   (c1    :initform #1=?/)
   (c2    :initform #2=?\\)
   (c3    :initform #1#)
   (c4    :initform #2#)
   (s1    :initform #3=`(,bad-nonsolid))
   (s2    :initform #4='(?.))
   (s3    :initform #3#)
   (s4    :initform #4#)
   (spc   :initform #3#)
   (outer      :initarg :outer      :type   (or null elem) :initform nil)
   (io         :initarg :io         :custom boolean        :initform nil)
   (decode     :initarg :decode     :custom boolean        :initform nil)
   (status     :initarg :status     :type   string         :initform "")
   (weight     :initarg :weight     :type   number         :initform 18)
   (weight-max :initarg :weight-max :type   number         :initform 44)))

(cl-defmethod bad-find-reset ((m magni-glass))
  (with-slots (s1 s3 status) m
    (setf s1 #1=`(,bad-nonsolid))
    (setf s3 #1#)
    (setf status ""))
  (bad-update m))

(cl-defmethod bad-string-is-data ((m magni-glass) (s string))
  (let ((data (string-to-list s)))
    (if (cl-every (lambda (c) (or (= c ?~) (= c ?-))) data)
      (with-slots (io) m
        (if io
            (progn
              #1=(seq--into-string
                  (cl-substitute ?1 ?~
                   (cl-substitute ?0 ?- data)))
              (message (format "char is: %c" (string-to-number #1# 2)))
              #1#)
          "i/o err."))
      s)))

(cl-defmethod bad-find ((m magni-glass))
  (with-slots (s1 s3 status decode outer io) m
    (setf s1 (list ?'))
    (setf s3 (list ?.))
    (bad-update m)
    (let* ((str (seq--into-string (bad-write-replace outer m t)))
           (len (length str)))
      (setf status
            (format "data: [%s]" (if (and (= len 2)
                                          decode
                                          (string= str "io"))
                                     (progn
                                       (setf io t)
                                       (message "i/o up!"))
                                   (if (= len 8)
                                       (if (and (not decode)
                                                (string= str "[      ]"))
                                           (progn
                                             (setf decode t)
                                             "lens up!")
                                         (if decode
                                             (bad-string-is-data m str)
                                           (seq-sort
                                            (lambda (_ __) (zerop (random 2)))
                                            "....xz@#")))
                                     (if (< len 8)
                                         (make-string 8 ?.)
                                       (make-string 8 ?*)))))))))

(cl-defmethod bad-is-movable ((m magni-glass))
  (with-slots (w h weight weight-max movable) m
    (setf movable (<= weight weight-max))))

(cl-defmethod bad-make-movable ((m magni-glass))
  (with-slots (movable) m
    (setf movable t)))

(cl-defmethod bad-set-area ((m magni-glass))
  (with-slots ((ow w) (oh h)) (oref m outer)
    (with-slots (w h max-x max-y) m
      (setf max-x (- ow w))
      (setf max-y (- oh h)))))

(cl-defmethod bad-pos  :after ((m magni-glass) &rest _)
  (bad-find-reset m))

(cl-defmethod bad-size :after ((m magni-glass) &rest _)
  (bad-find-reset m)
  (bad-set-area m)
  (with-slots (w h weight) m
    (setq weight (* w h)))
  (bad-is-movable m))

(provide 'bad-toronto-magni)
