;;; -*- lexical-binding: t -*-

(require 'eieio)

(require 'bad-box)
(require 'bad-elem)

(cl-defmethod bad-draw-endpoint-line ((e elem) l m r len &optional n)
  (or n (setq n 1))
  (when (<= (oref e w-min) len)
    (cl-loop
     with len-l = (length l)
     with len-r = (length r)
     with len-m = (- len 2)
     for i from 0 to (1- n)
     for l-item = (nth (mod i len-l) l)
     for r-item = (nth (mod i len-r) r)
     for m-item = (bad-string-repeat m len-m)
     collect (format "%c%s%c" l-item m-item r-item))))

(cl-defmethod bad-update ((b box))
  (with-slots (w h data len c1 c2 c3 c4 s1 s2 s3 s4 spc) b
    (let* ((tl (bad-draw-endpoint-line b (list c1) s1  (list c2) w))
           (ml (bad-draw-endpoint-line b s4        spc       s2  w  (- h 2)))
           (bl (bad-draw-endpoint-line b (list c4) s3  (list c3) w))
           (all `(,@tl ,@ml ,@bl))
           (str (string-join all)))
      (setf data (string-to-list str))
      (setf len (length data)))
    (bad-tick b)))

(provide 'bad-box-draw)
