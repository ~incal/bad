;;; -*- lexical-binding: t -*-

(require 'eieio)

(require 'bad-elem)
(require 'bad-paths)
(require 'bad-pos)
(require 'bad-write)

(defclass ascii (elem)
  ((name :initform "ascii")))

(cl-defmethod bad-make-transparent ((a ascii))
  (with-slots (data) a
    (setf data (cl-substitute bad-nonsolid ?\s data))))

(cl-defmethod bad-read-data ((a ascii) (strs list) &optional solid)
  (with-slots (w h data len) a
    (setf data (string-to-list (string-join strs)))
    (setf len  (length data))
    (setf w    (length (car strs)))
    (setf h    (length strs)))
  (unless solid
    (bad-make-transparent a)))

(cl-defmethod bad-read-file ((a ascii) &optional f dir solid)
  (with-slots (name) a
    (or dir (setq dir (file-name-concat bad-dir "data")))
    (setf f (file-name-concat dir (or f (concat name ".txt")))))
  (with-temp-buffer
    (insert-file-contents f)
    (let* ((lines (split-string
                   (buffer-substring-no-properties (point-min) (point-max))
                   "\n"))
           (len (apply #'max (mapcar #'length lines))))
      (bad-read-data a (mapcar (lambda (l) (string-pad l len)) lines) solid))))

(provide 'bad-ascii)
