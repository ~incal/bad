;;; -*- lexical-binding: t -*-

(require 'cl-lib)

(defvar bad-dir)

(defun bad--load-path-dir-and-subdirs (from)
  (let ((default-directory (file-name-directory from)))
    (cl-pushnew default-directory load-path)
    (normal-top-level-add-subdirs-to-load-path)))

(when load-file-name
  (setq bad-dir (file-name-directory load-file-name))
  (bad--load-path-dir-and-subdirs load-file-name))

(provide 'bad-paths)
