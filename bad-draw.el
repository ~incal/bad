;;; -*- lexical-binding: t -*-

(require 'eieio)

(require 'bad-elem)
(require 'bad-write)

(cl-defmethod bad-update ((_ elem)) (ignore))

(cl-defmethod bad-visible ((e elem) &optional set-to)
  (or set-to (setq set-to 0))
  (with-slots (visible) e
    (cl-case set-to
      (1 (setf visible t))
      (0 (setf visible (not visible)))
      (_ (setf visible nil)))))

(cl-defmethod bad-draw-to-buf-3 ((e elem))
  (with-slots (x y w h data fg color visible) e
    (when visible
      (goto-char (point-min))
      (forward-line y)
      (forward-char x)
      (cl-loop
       with end-col = (+ x w)
       ;; for bg-fg = nil
       ;; for bg-bg = nil
       for c in data do
       (if (= c bad-nonsolid)
           (if (not (eobp))
               (forward-char)
             (insert c))
         (unless (eobp)
           ;; (setq bg-fg (foreground-color-at-point))
           ;; (setq bg-bg (background-color-at-point))
           (delete-char 1))
         (insert c)
         (put-text-property (1- #1=(point)) #1# 'face
                            (list :foreground fg :background color)))
       (when (= (current-column) end-col)
         (goto-char (+ x (pos-bol 2)))))
      (goto-char (point-min)))))

(cl-defmethod bad-draw-to-buf-transparent ((e elem) &optional kind boost-x boost-y)
  (or boost-x (setq boost-x 0))
  (or boost-y (setq boost-y 0))
  (with-slots (x y w h data fg color visible) e
    (let ((bx (+ x boost-x))
          (by (+ y boost-y)))
    (when visible
      (goto-char (point-min))
      (forward-line by)
      (forward-char bx)
      (cl-loop
       for c in data
       for prop = nil
       with beg = (point)
       with end-col = (+ bx w)
       with eol
       with l = 1 do
       (if (= c bad-nonsolid)
           (if (not (eobp))
               (forward-char)
             (insert c))
         (unless (eobp)
           (when kind
             (setq prop (list :foreground fg
                              :background (background-color-at-point))))
           (delete-char 1))
         (insert c)
         (when (and kind prop)
           (put-text-property (1- #1=(point)) #1# 'face prop)))
       (when (= (current-column) end-col)
         (unless (<= h l)
           (setq eol (+ beg w))
           (unless (or (= l 1) (not fg) (not color))
             (put-text-property (1+ beg) (1- eol) 'face
                                (list :foreground fg :background color))))
         (cl-incf l)
         (goto-char (+ bx (pos-bol 2)))
         (setq beg (point))))))
    (goto-char (point-min))))

(cl-defmethod bad-draw-to-buf ((e elem) &optional keep)
  (with-slots (data color w h visible) e
    (when visible
      (if keep
          (goto-char (point-min))
        (erase-buffer))
      (cl-loop
       with chunks = (take h (seq-split data w))
       for c in chunks
       for s = (seq--into-string c) do
       (if (or (not color) (string= "" color))
           (insert (format "%s\n" s))
         (insert (propertize (format "%s\n" s) 'face (list :background color)))))
      (unless keep
        (goto-char (point-min))))))

(provide 'bad-draw)
