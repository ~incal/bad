;;; -*- lexical-binding: t -*-

(require 'eieio)

(require 'bad-elem)
(require 'bad-pos)

(cl-defmethod bad-mov ((e elem) (dx integer) (dy integer))
  (with-slots (x y) e
    (bad-pos e (+ dx x) (+ dy y))))

;; -----------------------------------------------------------------------

(cl-defmethod bad-mov-x ((e elem) &optional d)
  (or d (setq d 1))
  (bad-mov e d 0))

(cl-defmethod bad-mov-y ((e elem) &optional d)
  (or d (setq d 1))
  (bad-mov e 0 d))

;; -----------------------------------------------------------------------

(cl-defmethod bad-up ((e elem) &optional d)
  (or d (setq d 1))
  (bad-mov-y e (- d)))

(cl-defmethod bad-down ((e elem) &optional d)
  (or d (setq d 1))
  (bad-mov-y e d))

(cl-defmethod bad-left ((e elem) &optional d)
  (or d (setq d 1))
  (bad-mov-x e (- d)))

(cl-defmethod bad-right ((e elem) &optional d)
  (or d (setq d 1))
  (bad-mov-x e d))

(provide 'bad-move)
