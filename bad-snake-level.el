;;; -*- lexical-binding: t -*-

(require 'eieio)

(defclass snake-level ()
  ((name      :initarg :name      :type string  :initform "snake-level" )
   (lvl       :initarg :lvl       :type integer :initform 1)
   (score-min :initarg :score-min :type integer :initform 0)
   (score-max :initarg :score-max :type integer :initform 100)))

(provide 'bad-snake-level)
