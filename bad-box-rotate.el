;;; -*- lexical-binding: t -*-

(require 'eieio)
(require 'bad-box)

(cl-defmethod bad-rotate-corners ((b box) &optional n)
  (or n (setq n 1))
  (with-slots (c1 c2 c3 c4) b
    (if (< 0 n)
        (dotimes (_ n)
          (cl-rotatef c4 c3 c2 c1))
      (dotimes (_ (abs n))
        (cl-rotatef c1 c2 c3 c4)))))

(cl-defmethod bad-rotate-sides ((b box) &optional n)
  (or n (setq n 1))
  (with-slots (s1 s2 s3 s4) b
    (if (< 0 n)
        (dotimes (_ n)
          (cl-rotatef s4 s3 s2 s1))
      (dotimes (_ (abs n))
        (cl-rotatef s1 s2 s3 s4)))))

(cl-defmethod bad-rotate-border ((b box) &optional n)
  (or n (setq n 1))
  (bad-rotate-corners b n)
  (bad-rotate-sides b n))

(cl-defmethod bad-rotate :after ((b box) &optional n inner)
  (or n (setq n 1))
  (when (oref b movable)
    (let ((tms (if inner (* 3 n) n)))
      (bad-rotate-border b tms)))
  (bad-update b))

(provide 'bad-box-rotate)
