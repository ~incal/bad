;;; -*- lexical-binding: t -*-

(require 'bad-box)
(require 'bad-box-draw)
(require 'bad-draw)
(require 'bad-triangle)
(require 'bad-size)
(require 'bad-move)
(require 'bad-write)

(defun bad-draw-here ()
  (interactive)
  (let ((bg  (box :w 11 :h 9))
        (tri (triangle)))
    (bad-update bg)

    (with-slots (h) bg
      (bad-size tri (- h 2)))
    (bad-mov  tri 2 1)
    (bad-write-replace bg tri)

    (dotimes (_ 3)
      (bad-draw-to-buf bg t)))
  (goto-char (point-min)))

(provide 'bad-demo-draw-here)
