;;; -*- lexical-binding: t -*-

(defun bad-colorize-at-point (&optional fg bg beg end)
  (interactive)
  (or fg (setq fg "green"))
  (or bg (setq bg "black"))
  (or beg (setq beg (point)))
  (or end (setq end (1+ beg)))
  (put-text-property beg end 'face (list :foreground fg :background bg)))

(provide 'bad-color)
