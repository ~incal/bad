;;; -*- lexical-binding: t -*-

(require 'eieio)

(require 'bad-elem)
(require 'bad-write)

(defun bad-string-repeat (chrs len)
  (when (listp chrs)
    (let ((str (seq--into-string chrs)))
      (while (< (length str) len)
        (setq str (format "%s%s" str str)))
      (substring str 0 len))))

(defclass borderless (elem)
  ((name  :initform (format "borderless-%d" (random 100)))
   (w-min :initform #1=2)
   (h-min :initform #2=2)
   (w     :initform #1#)
   (h     :initform #2#)
   (spc   :initarg :spc :type list :initform '(?\s))))

(cl-defmethod bad-resize-sub :after ((b borderless))
  (with-slots (sub w h) b
    (dolist (s sub)
      (with-slots (w-max h-max) s
        (setf w-max w)
        (setf h-max h)))))

(cl-defmethod bad-size :after ((b borderless) &rest _)
  (bad-update b))

(cl-defmethod bad-update ((b borderless))
  (with-slots (w h data len spc) b
    (setf len (* w h))
    (setf data (string-to-list (bad-string-repeat spc len))))
  (bad-tick b))

(cl-defmethod bad-tick ((b borderless))
  (with-slots (sub tick) b
    (dolist (s sub)
      (bad-update s))
    (bad-write-subs b)
    (when (functionp tick) ; NOTE: OK
      (funcall tick))))

(provide 'bad-borderless)
