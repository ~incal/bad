;;; -*- lexical-binding: t -*-
;;
;; ┏━━━━━━━━━┓
;; ┃ ####### ┃
;; ┃  ###### ┃
;; ┃ G ##### ┃
;; ┃ N  #### ┃
;; ┃ U   ### ┃
;; ┃      ## ┃
;; ┃ Emacs # ┃
;; ┗━━━━━━━━━┛

(require 'bad-box)
(require 'bad-demo)
(require 'bad-draw)
(require 'bad-move)
(require 'bad-rotate)
(require 'bad-size)
(require 'bad-triangle)
(require 'bad-write)

(defun bad-run-mini ()
  (let ((bg  (box :w #1=11 :h #2=9 :s1 #3='(?━) :s3 #3#))
        (tri (triangle)))
    (bad-update bg)

    (bad-size tri (- #2# 2))
    (bad-mov  tri 2 1)
    (bad-rotate tri)
    (bad-update tri)
    (bad-write-replace bg tri)

    (let* ((line #1#)
           (g (+ #1=2 (* 3 line)))
           (n (+ #1#  (* 4 line)))
           (u (+ #1#  (* 5 line)))
           (i))
      (bad-write-index bg g ?G)
      (bad-write-index bg n ?N)
      (bad-write-index bg u ?U)

      (setq i u)
      (bad-write-index bg (cl-incf i (* 2 line)) ?E)
      (bad-write-index bg (cl-incf i)            ?m)
      (bad-write-index bg (cl-incf i)            ?a)
      (bad-write-index bg (cl-incf i)            ?c)
      (bad-write-index bg (cl-incf i)            ?s)

      (let ((buf (get-buffer-create "*bad-demo-buffer*")))
        (with-current-buffer buf
          (bad-demo-prepare)
          (bad-draw-to-buf bg)
          (put-text-property #1=(+ 4 g)            (+ 1 #1#) 'face `(:foreground "cyan"))
          (put-text-property #2=(+ 5 g      line)  (+ 1 #2#) 'face `(:foreground "magenta"))
          (put-text-property #3=(+ 6 g (* 2 line)) (+ 1 #3#) 'face `(:foreground "yellow"))
          (put-text-property #4=(+ 8 g (* 4 line)) (+ 5 #4#) 'face `(:foreground "green")))
        (pop-to-buffer buf)
        (delete-other-windows))))

  (goto-char (point-max))
  (message "Press [q] proceed"))

(provide 'bad-demo-mini)
