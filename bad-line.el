;;; -*- lexical-binding: t -*-

(require 'eieio)

(require 'bad-elem)
(require 'bad-box-draw)
(require 'bad-size)

(defclass line (elem)
  ((name  :initform (format "line-%d" (random 100)))
   (se  :initarg :se :type   integer :initform #1=?*)
   (be  :initarg :be :type   integer :initform #1#)
   (vm  :initarg :vm :type   list    :initform '(?|))
   (hm  :initarg :hm :type   list    :initform '(#2=?-))
   (xx  :initarg :md :custom boolean :initform t)
   (data  :initform #3=(list #1# #2# #1#))
   (w     :initform #4=(length #3#))
   (h     :initform 1)
   (len   :initform #4#)))

(cl-defmethod bad-size :after ((l line) &rest _)
  (bad-update l))

(cl-defmethod bad-update ((l line))
  (with-slots (se be xx hm vm data len w h) l
    (setf data
          (string-to-list
           (car (bad-draw-endpoint-line l (list se)
                                          (if xx hm vm)
                                          (list be)
                                          (if xx w h) ))))
    (setf len (length data))
    ))

(cl-defmethod bad-line-endpoint ((l line) &optional set-se set-be)
  (when (or set-se set-be)
    (with-slots (se be) l
      (when set-se
        (setf se set-se))
      (when set-be
        (setf be set-be)))
    (bad-update l)))

(cl-defmethod bad-line-mid ((l line) &optional set-vm set-hm)
  (when (or set-vm set-hm)
    (with-slots (vm hm) l
      (when set-vm
        (setf vm set-vm))
      (when set-hm
        (setf hm set-hm)))
    (bad-update l)))

(provide 'bad-line)
